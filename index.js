/*
 * Copyright (c) 2006-present eyeo GmbH
 *
 * This module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {Chromium} from "./src/chromium.js";
import {Firefox} from "./src/firefox.js";
import {Edge} from "./src/edge.js";

export {download, takeFullPageScreenshot, snapshotsBaseDir, getMajorVersion}
  from "./src/utils.js";

/**
 * @type {Object}
 * @property {Chromium} chromium Browser and webdriver functionality for
 *   Chromium.
 * @property {Firefox} firefox Browser and webdriver functionality for Firefox.
 * @property {Edge} edge Browser and webdriver functionality for Edge.
 */
export const BROWSERS = {
  chromium: Chromium,
  firefox: Firefox,
  edge: Edge
};
