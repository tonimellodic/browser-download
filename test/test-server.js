/*
 * Copyright (c) 2006-present eyeo GmbH
 *
 * This module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from "path";
import url from "url";
import express from "express";

const HOST = "localhost";
const PORT = 3000;

export const TEST_SERVER_URL = `http://${HOST}:${PORT}`;

let app = express();
let dirname = path.dirname(url.fileURLToPath(import.meta.url));

app.use(express.static(path.join(dirname, "pages")));

export function startTestServer() {
  app.listen(PORT, HOST, () => {
    // eslint-disable-next-line no-console
    console.log(`Test server listening at ${TEST_SERVER_URL}`);
  });
}
