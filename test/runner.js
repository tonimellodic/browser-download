import path from "path";
import {spawn, execSync} from "child_process";

let testServerProcess;

export function runTestServer() {
  if (testServerProcess)
    return;

  testServerProcess =
    spawn("node", [path.join(process.cwd(), "test", "start-server.js")]);

  return new Promise((resolve, reject) => {
    function onData(data) {
      console.log(data.toString()); // eslint-disable-line no-console

      let testServerStarted = data.includes("listening at");
      if (testServerStarted)
        resolve();
    }

    function onClose(code) {
      if (code != 0)
        reject(new Error("Failed to start test server"));
    }

    testServerProcess.stderr.on("data", onData);
    testServerProcess.stdout.on("data", onData);
    testServerProcess.on("close", onClose);
  });
}

export async function killTestServer() {
  if (!testServerProcess)
    return;

  await new Promise((resolve, reject) => {
    let timeoutID = setTimeout(() => {
      reject(new Error("Could not stop test server"));
    }, 5000);

    function onClose() {
      testServerProcess.off("close", onClose);
      resolve();
      clearTimeout(timeoutID);
    }

    testServerProcess.on("close", onClose);
    testServerProcess.kill("SIGKILL");
  });

  testServerProcess = null;
}

async function run() {
  let args = process.argv.slice(2); // Keep npm args from "--" onwards
  args = args.map(v => v.startsWith("-") ? v : `"${v}"`);

  await runTestServer();
  try {
    execSync(`npm run test-suite ${args.join(" ")}`, {stdio: "inherit"});
  }
  finally {
    await killTestServer();
  }
}

(async() => {
  try {
    await run();
  }
  catch (err) {
    console.error(err instanceof Error ? err.stack : `Error: ${err}`);
    process.exit(1);
  }
})();
